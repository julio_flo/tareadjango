import numpy as np
import cv2



cap = cv2.VideoCapture(0)


while (True):
	ret, frame = cap.read()

	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	edge = cv2.Canny(gray,60,100)
	

	cv2.imshow('video', frame)
	cv2.imshow('filtro blanco y negro', gray)
	cv2.imshow('filtro edge', edge)



	cv2.waitKey(1) 


cv2.destroyAllWindows()